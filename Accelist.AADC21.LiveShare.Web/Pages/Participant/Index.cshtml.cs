﻿using Accelist.AADC21.LiveShare.Web.Data;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Accelist.AADC21.LiveShare.Web.Pages.Participant;

public class IndexModel : PageModel
{
    private readonly InMemoryProvider _db;
    public List<Data.Models.ParticipantViewModel> Participants = new List<Data.Models.ParticipantViewModel>();

    public IndexModel(InMemoryProvider db)
    {
        _db = db;
    }

    public void OnGet()
    {
        // Simple join query using LINQ on in-memory data.
        var participantQuery = from s in _db.Sessions
                   join p in _db.Participants on s.Id equals p.SessionId
                   select new Data.Models.ParticipantViewModel
                   {
                       SessionId = s.Id,
                       SessionName = s.Name,
                       ParticipantId = p.Id,
                       ParticipantName = p.Name,
                       Company = p.Company
                   };

        Participants = participantQuery.ToList();
    }
}
