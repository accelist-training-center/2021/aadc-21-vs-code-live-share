﻿using Accelist.AADC21.LiveShare.Web.Data;
using Accelist.AADC21.LiveShare.Web.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Accelist.AADC21.LiveShare.Web.Pages.Participant;

public class CreateModel : PageModel
{
    private readonly InMemoryProvider _db;
    public readonly List<SelectListItem> Sessions;

    [BindProperty]
    public CreateParticipantForm Form { get; set; } = new CreateParticipantForm();

    public CreateModel(InMemoryProvider db)
    {
        _db = db;
        Sessions = _db.Sessions.Select(Q => new SelectListItem
        {
            Value = Q.Id.ToString(),
            Text = Q.Name
        })
        .ToList();
    }

    public void OnGet()
    {

    }

    public ActionResult OnPost()
    {
        if (ModelState.IsValid == false)
        {
            return Page();
        }

        var lastParticipant = _db.Participants.LastOrDefault();
        var lastParticipantId = 0;

        if (lastParticipant != null)
        {
            lastParticipantId = lastParticipant.Id;
        }

        lastParticipantId++;

        var newParticipant = new Data.Models.Participant
        {
            Id = lastParticipantId,
            Name = Form.ParticipantName,
            Company = Form.Company,
            SessionId = Form.SessionId
        };

        _db.Participants.Add(newParticipant);

        return RedirectToPage("/Participant/Index");
    }
}
