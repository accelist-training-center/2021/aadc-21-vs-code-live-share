﻿using Accelist.AADC21.LiveShare.Web.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Accelist.AADC21.LiveShare.Web.Pages.Session;

public class IndexModel : PageModel
{
    private readonly InMemoryProvider _db;
    public readonly List<Data.Models.Session> _sessions;

    public IndexModel(InMemoryProvider db)
    {
        _db = db;
        _sessions = _db.Sessions;
    }

    public void OnGet()
    {

    }
}
