﻿using Accelist.AADC21.LiveShare.Web.Data;
using Accelist.AADC21.LiveShare.Web.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Accelist.AADC21.LiveShare.Web.Pages.Session;

public class CreateModel : PageModel
{
    private readonly InMemoryProvider _db;

    [BindProperty]
    public CreateSessionForm Form { get; set; } = new CreateSessionForm
    {
        StartTime = DateTimeOffset.Now,
        FinishTime = DateTimeOffset.Now
    };

    public CreateModel(InMemoryProvider db)
    {
        _db = db;
    }

    public void OnGet()
    {

    }

    public ActionResult OnPost()
    {
        if (ModelState.IsValid == false)
        {
            return Page();
        }

        var lastSession = _db.Sessions.LastOrDefault();
        var lastSessionId = 1;

        if (lastSession != null)
        {
            lastSessionId = lastSession.Id;
        }

        lastSessionId++;

        var newSession = new Data.Models.Session
        {
            Id = lastSessionId,
            Name = Form.SessionName,
            SpeakerName = Form.SpeakerName,
            StartTime = Form.StartTime,
            FinishTime = Form.FinishTime
        };

        _db.Sessions.Add(newSession);

        return RedirectToPage("/Session/Index");
    }
}
