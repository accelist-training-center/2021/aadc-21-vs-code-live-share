using Accelist.AADC21.LiveShare.Web.Data.Models;
using Accelist.AADC21.LiveShare.Web.Constants;

namespace Accelist.AADC21.LiveShare.Web.Data
{
    /// <summary>
    /// In-memory database for storing this web app data.
    /// </summary>
    public class InMemoryProvider
    {
        /// <summary>
        /// Gets or sets the session list.
        /// </summary>
        public List<Session> Sessions { get; set; } = new List<Session>
        {
            new Session {
                Id = 1,
                Name = "Opening",
                SpeakerName = "Matius Kelvin, Markus Fresnel",
                StartTime = new DateTimeOffset(2021, 12, 11, 9, 0, 0, Timezones.JakartaTimezone),
                FinishTime = new DateTimeOffset(2021, 12, 11, 11, 0, 0, Timezones.JakartaTimezone),
            }
        };

        /// <summary>
        /// Gets or sets the participant list.
        /// </summary>
        public List<Participant> Participants { get; set; } = new List<Participant>
        {
            new Participant 
            {
                Id = 1,
                Name = "Admin",
                Company = "Accelist",
                SessionId = 1
            }
        };
    }
}
