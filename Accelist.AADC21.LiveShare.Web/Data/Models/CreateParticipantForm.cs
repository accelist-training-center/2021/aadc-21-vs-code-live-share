using System.ComponentModel.DataAnnotations;

namespace Accelist.AADC21.LiveShare.Web.Data.Models
{
    /// <summary>
    /// Model class for binding create participant form data.
    /// </summary>
    public class CreateParticipantForm
    {
        /// <summary>
        /// Gets or sets session's ID.
        /// </summary>
        [Required]
        public int SessionId { get; set; }

        /// <summary>
        /// Gets or sets participant's name.
        /// </summary>
        [Required]
        public string? ParticipantName { get; set; }

        /// <summary>
        /// Gets or sets the participant's company name.
        /// </summary>
        [Required]
        public string? Company { get; set; }
    }
}
