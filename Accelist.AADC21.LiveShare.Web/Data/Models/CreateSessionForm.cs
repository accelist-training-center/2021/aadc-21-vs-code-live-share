using System.ComponentModel.DataAnnotations;

namespace Accelist.AADC21.LiveShare.Web.Data.Models
{
    /// <summary>
    /// Model class for binding create session form data.
    /// </summary>
    public class CreateSessionForm
    {
        /// <summary>
        /// Gets or sets the session's name.
        /// </summary>
        [Display(Name = "Session")]
        [Required]
        public string? SessionName { get; set; }

        /// <summary>
        /// Gets or sets the speaker's name.
        /// </summary>
        [Display(Name = "Speaker")]
        [Required]
        public string? SpeakerName { get; set; }

        /// <summary>
        /// Gets or sets the session's start time.
        /// </summary>
        [Display(Name = "Start")]
        [Required]
        public DateTimeOffset StartTime { get; set; }

        /// <summary>
        /// Gets or sets the session's finish time.
        /// </summary>
        [Display(Name = "Finish")]
        [Required]
        public DateTimeOffset FinishTime { get; set; }
    }
}
