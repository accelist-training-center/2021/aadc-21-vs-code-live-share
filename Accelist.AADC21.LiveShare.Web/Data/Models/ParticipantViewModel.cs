namespace Accelist.AADC21.LiveShare.Web.Data.Models
{
    /// <summary>
    /// Model class for binding the participant data.
    /// </summary>
    public class ParticipantViewModel
    {
        /// <summary>
        /// Gets or sets the session's ID.
        /// </summary>
        public int SessionId { get; set; }

        /// <summary>
        /// Gets or sets the participant's ID.
        /// </summary>
        public int ParticipantId { get; set; }

        /// <summary>
        /// Gets or sets the session's name.
        /// </summary>
        public string? SessionName { get; set; }

        /// <summary>
        /// Gets or sets the participant's name.
        /// </summary>
        public string? ParticipantName { get; set; }

        /// <summary>
        /// Gets or sets the company name.
        /// </summary>
        public string? Company { get; set; }
    }
}
