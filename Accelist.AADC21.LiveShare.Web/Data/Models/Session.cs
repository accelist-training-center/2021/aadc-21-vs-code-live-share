namespace Accelist.AADC21.LiveShare.Web.Data.Models
{
    /// <summary>
    /// Model class for storing the AADC21 session data.
    /// </summary>
    public class Session
    {
        /// <summary>
        /// Gets or sets the session's ID.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the session's name.
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// Gets or sets the speaker name of this session.
        /// </summary>
        public string? SpeakerName { get; set; }

        /// <summary>
        /// Gets or sets the start time of this session.
        /// </summary>
        public DateTimeOffset StartTime { get; set; }

        /// <summary>
        /// Gets or sets the finish time of this session.
        /// </summary>
        public DateTimeOffset FinishTime { get; set; }
    }
}
