namespace Accelist.AADC21.LiveShare.Web.Data.Models
{
    /// <summary>
    /// Model class for storing the AADC21 participant data.
    /// </summary>
    public class Participant
    {
        /// <summary>
        /// Gets or sets the participant's ID.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the participant's name.
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// Gets or sets the participant's company name.
        /// </summary>
        public string? Company { get; set; }

        /// <summary>
        /// Gets or sets the session ID that this participant is following.
        /// </summary>
        public int SessionId { get; set; }
    }
}
