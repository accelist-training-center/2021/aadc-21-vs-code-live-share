# AADC 21 VS Code Live Share

Example source codes for VS Code Live Share session.

## Installation

### Cloning the Repository

To clone the repository, run this following command: `git clone https://gitlab.com/accelist-training-center/2021/aadc-21-vs-code-live-share.git`

### Developer Tools
To begin the development of the application, you must install these following softwares:
| Name | Version | Type | DL Link | Notes |
| ---- | ------- | ---- | ------- | ----- |
| .NET SDK | 6 | SDK | https://dotnet.microsoft.com/en-us/download/dotnet/6.0 | |
| Visual Studio Code | | IDE | | (Optional) you could use other IDE or code editor. |

### Running the Application
You can running the application by using `dotnet run` command or run with debugger on your VS Code (hotkey: F5).